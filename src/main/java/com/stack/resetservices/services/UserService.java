package com.stack.resetservices.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.stack.resetservices.entities.User;
import com.stack.resetservices.exception.UserCreationException;
import com.stack.resetservices.exception.UserNotFoundException;
import com.stack.resetservices.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<User> getAllUser(){
		return userRepository.findAll();
	}
	
	public User createUser(User user) throws UserCreationException {
		
		User checkUser = userRepository.findByUserName(user.getUserName());
		if(checkUser !=null) {
			throw new UserCreationException("User Already Exist");
		}
		
		return userRepository.save(user);
	}
	
	public Optional<User> getUserById(Long id) throws UserNotFoundException{
		Optional<User> user = userRepository.findById(id);
		
		if(!user.isPresent()) {
			throw new UserNotFoundException("OOPS! Who are you?");
		}
		return user;
	}
	
	public User updateUserById(Long id, User user) throws UserNotFoundException {

		Optional<User> optionalUser = userRepository.findById(id);
		
		if(!optionalUser.isPresent()) {
			throw new UserNotFoundException("OOPS! User Not Found");
		}
		
		user.setId(id);
		return userRepository.save(user);
	}
	
	public String deleteUserById(Long id) {
		Optional<User> optionalUser = userRepository.findById(id);
		
		if(!optionalUser.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User Not Found");
		}
		userRepository.deleteById(id);
		return "User Deleted Successfully";
	}
	
	public User getUserByUserName(String username) {
		return userRepository.findByUserName(username);
	}
}
